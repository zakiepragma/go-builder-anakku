package main

// import (
// 	"fmt"
// 	"time"
// )

// type Anak struct {
// 	AnakKe       int
// 	Nama         string
// 	HariLahir    string
// 	TanggalLahir string
// 	JamLahir     string
// 	TempatLahir  string
// 	AlamatLahir  string
// }

// func (anak *Anak) newBorn(today string) {
// 	if today == anak.TanggalLahir {
// 		fmt.Printf("Alhamdulillah, telah lahir anak ke-%d kami pada hari %s, %s pukul %s di %s, %s. InsyaAllah akan kami beri nama %s.\n", anak.AnakKe, anak.HariLahir, anak.TanggalLahir, anak.JamLahir, anak.TempatLahir, anak.AlamatLahir, anak.Nama)
// 	}
// }

// func main() {
// 	today := time.Now().Format("02-01-2006")

// 	anak1 := Anak{AnakKe: 1, Nama: "Al Muzani", HariLahir: "Jum'at", TanggalLahir: "25-08-2023", JamLahir: "11:14 WIB", TempatLahir: "Bidan Juleha", AlamatLahir: "Jl Delima 7 Panam, Pekanbaru"}
// 	anak2 := Anak{AnakKe: 2, Nama: "El Khuzaimah", HariLahir: "Ahad", TanggalLahir: "15-09-2024", JamLahir: "11:14 WIB", TempatLahir: "RSIA Annisa", AlamatLahir: "Jl Garuda No.66, Pekanbaru"}

// 	anak1.newBorn(today)
// 	anak2.newBorn(today)
// }
