package main

import (
	"fmt"
	"time"
)

type Anak struct {
	AnakKe       int
	Nama         string
	HariLahir    string
	TanggalLahir string
	JamLahir     string
	TempatLahir  string
	AlamatLahir  string
}

type AnakBuilder struct {
	anak *Anak
}

func NewAnakBuilder() *AnakBuilder {
	return &AnakBuilder{&Anak{}}
}

func (b *AnakBuilder) SetAnakKe(anakKe int) *AnakBuilder {
	b.anak.AnakKe = anakKe
	return b
}

func (b *AnakBuilder) SetNama(nama string) *AnakBuilder {
	b.anak.Nama = nama
	return b
}

func (b *AnakBuilder) SetHariLahir(hariLahir string) *AnakBuilder {
	b.anak.HariLahir = hariLahir
	return b
}

func (b *AnakBuilder) SetTanggalLahir(tanggalLahir string) *AnakBuilder {
	b.anak.TanggalLahir = tanggalLahir
	return b
}

func (b *AnakBuilder) SetJamLahir(jamLahir string) *AnakBuilder {
	b.anak.JamLahir = jamLahir
	return b
}

func (b *AnakBuilder) SetTempatLahir(tempatLahir string) *AnakBuilder {
	b.anak.TempatLahir = tempatLahir
	return b
}

func (b *AnakBuilder) SetAlamatLahir(alamatLahir string) *AnakBuilder {
	b.anak.AlamatLahir = alamatLahir
	return b
}

func (b *AnakBuilder) Build() *Anak {
	return b.anak
}

func (anak *Anak) newBorn() {
	today := time.Now().Format("02-01-2006")

	if today == anak.TanggalLahir {
		fmt.Printf("Alhamdulillah, telah lahir anak ke-%d kami pada hari %s, %s pukul %s di %s, %s. InsyaAllah akan kami beri nama %s.\n", anak.AnakKe, anak.HariLahir, anak.TanggalLahir, anak.JamLahir, anak.TempatLahir, anak.AlamatLahir, anak.Nama)
	}
}

func main() {
	anak1 := NewAnakBuilder().
		SetAnakKe(1).
		SetNama("Al Muzani").
		SetHariLahir("Jum'at").
		SetTanggalLahir("25-08-2023").
		SetJamLahir("11:14 WIB").
		SetTempatLahir("BPM Siti Julaeha").
		SetAlamatLahir("Jl Delima 7 Panam, Pekanbaru").
		Build()

	anak2 := NewAnakBuilder().
		SetAnakKe(2).
		SetNama("El Khuzaimah").
		SetHariLahir("Ahad").
		SetTanggalLahir("15-09-2024").
		SetJamLahir("11:14 WIB").
		SetTempatLahir("RSIA Annisa").
		SetAlamatLahir("Jl Garuda No.66, Pekanbaru").
		Build()

	anak1.newBorn()
	anak2.newBorn()
}
